#include <algorithm>
#include "numberconversion.h"

int lookup_nums[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
std::string lookup_strings[] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

int single_roman(char letter) {
    std::string s(1, letter);

    int index = std::find(lookup_strings, lookup_strings+13, s) - lookup_strings;
    
    if (index == 13)
      return 0;
    else
      return lookup_nums[index];
}

/*
  Example:

    roman_numeral = XIV
    X: total += 10
    I: total -= 1
    V: total += 5
    at the end total = 14
*/
int romantoint(std::string roman_numeral) {
    int total = 0, current_value = 0;

    for (size_t i = 0; i < roman_numeral.size(); i++) {
        current_value = single_roman(roman_numeral[i]);

        if (current_value < single_roman(roman_numeral[i + 1]))
            total -= current_value;
        else
            total += current_value;
    }

    return total;
}

/*
  Example:
    
    number = 14
    result = nothing
    14 >= 1000 ? no
    keep going...
    14 >= 10 ? yes
    so result is now X & number is now 4
    4 >= 10 ? no
    4 >= 4 ? yes
    so result is now XIV & number is now 0
*/

std::string inttoroman(int number) {
    std::string result;

    for (int i = 0; i < 13; i++) {
        while (number >= lookup_nums[i]) {
            result += lookup_strings[i];
            number -= lookup_nums[i];
        }
    }

    return result;
}

