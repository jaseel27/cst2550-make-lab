CXX=g++
CFLAGS=-Wall -Wextra

all: romandigitconverter

run: all
	./romandigitconverter

romandigitconverter: romandigitconverter.o numberconversion.o
	$(CXX) $(CFLAGS) numberconversion.o romandigitconverter.o -o romandigitconverter

romandigitconverter.o: numberconversion.o
	$(CXX) $(CFLAGS) romandigitconverter.cpp -c -o romandigitconverter.o

numberconversion.o: numberconversion.cpp
	$(CXX) $(CFLAGS) numberconversion.cpp -c -o numberconversion.o

clean:
	rm -f *.o
	rm -f romandigitconverter

.PHONY: clean all run
